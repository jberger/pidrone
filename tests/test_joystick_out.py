import time
import os
import sys

sys.path.append('../../Gamepad')
import Gamepad

def clear():
    os.system('clear')

def drange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step

def roundPartial(value, resolution):
    return round(value / resolution) * resolution

# Gamepad settings
gamepadType = Gamepad.PS3
joystick_x = 'LEFT-X'
joystick_y = 'LEFT-Y'
buttonExit = 'SELECT'

# Wait for a connection
if not Gamepad.available():
    print('Please connect your gamepad...')
    while not Gamepad.available():
        time.sleep(1.0)
gamepad = gamepadType()
print('Gamepad connected')

x = 0.0
y = 0.0
step_size = 0.25

while gamepad.isConnected():
    # Wait for the next event
    eventType, control, value = gamepad.getNextEvent()

    if eventType == 'BUTTON':
        if control == 'SELECT':
            if value:
                print('EXIT')
                break

    if eventType == 'AXIS':
        if control == 'LEFT-X':
            x = value
        elif control == 'LEFT-Y':
            y = value

    clear()

    x = roundPartial(x, step_size)
    y = roundPartial(y, step_size)

    for i in drange(-1.0, 1.0+step_size, step_size):
        for j in drange(-1.0, 1.0+step_size, step_size):
            if i == y and j == x:
                print(' X ', end='')
            else:
                print(' _ ', end='')
        print('\n', end='')