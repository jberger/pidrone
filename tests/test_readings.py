import sys
import signal
sys.path.append('../')
from MSPio.MultiWiiProtocol import MSPio

if __name__ == '__main__':
    ser = MSPio()

    # Make sure to clean up buffers and close connection, if terminating the app
    def signal_handler(signal, frame):
        print('\nCleaning up...')
        ser.cleanup()
        ser.close()
        sys.exit(0)
    signal.signal(signal.SIGINT, signal_handler)

    user_input = ''
    while user_input not in {'att', 'imu'}:
        user_input = str(input("Type desired readings ('att', 'imu'): "))

    if ser.is_open():
        ser._serial.read_all()

        # Print attitude readings
        if user_input == 'att':
            while True:
                print('Attitude readings: {0}'.format(ser.read_attitude()), end='\r')
                sys.stdout.flush()
        # Print altitude readings
        if user_input == 'imu':
            while True:
                print('IMU readings: {0}'.format(ser.read_imu()), end='\r')
                sys.stdout.flush()