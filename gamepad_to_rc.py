import time
import sys
from rc_transmitter import RCTransmitter

sys.path.append('Gamepad')
import Gamepad

# step_size: steps within range [1000,2000] taken after each polling interval for values controlled by buttons
# len_range: range around midpoint 1500 for ROLL/PITCH/YAW
# min_throttle: minimal throttle as in the flight configurator
# max_throttle: maximal throttle as in the flight configurator (or smaller for testing)
# hover_throttle: throttle, for which the drone reliably hovers
transmitter = RCTransmitter(step_size=5, len_range=[400, 400, 400], min_throttle=1150, max_throttle=1500, hover_throttle=1460)

# Gamepad settings
gamepadType = Gamepad.PS3
pollInterval = 0.05

# Wait for a connection
if not Gamepad.available():
    print('Please connect your gamepad...')
    while not Gamepad.available():
        time.sleep(1.0)
gamepad = gamepadType()
print('Gamepad connected')

# Set sinitial state
lx, ly = 0.0, 0.0
rx, ry = 0.0, 0.0
l2, r2 = -1.0, -1.0

start_time = time.time()

# Start the background updating
gamepad.startBackgroundUpdates()

# Gamepad events handled in the background
try:
    print("---------Controls---------")
    print("Stop app: CROSS")
    print("Arm: START")
    print("Disarm: SELECT")
    print("Roll/Pitch: Right Joystick")
    print("Yaw: L2/R2")
    print("Throttle: Left Joystick, Up/Down")
    print("ANGLE Mode: R1")
    print("--------------------------")
        
    # Main control loop
    while gamepad.isConnected() and not transmitter.in_failsafe_mode:
        # Arm/Disarm with START/SELECT
        if gamepad.beenPressed('START'):
            transmitter.arm()
        if gamepad.beenPressed('SELECT'):
            transmitter.disarm()

        # Exit application by pressing CROSS
        if gamepad.beenPressed('CROSS'):
            print('\nEXIT was pressed.')
            break

        # Enter ANGLE mode, if corresponding button was pressed
        if (gamepad.beenPressed('R1')):
            if transmitter.angle_mode():
                print("\nANGLE mode activated.")
            else:
                print("\nANGLE mode deactivated.")

        l2 = gamepad.axis('L2')
        r2 = gamepad.axis('R2')

        # Inverted y axes!
        lx = gamepad.axis('LEFT-X')
        ly = -gamepad.axis('LEFT-Y')
        rx = gamepad.axis('RIGHT-X')
        ry = -gamepad.axis('RIGHT-Y')

        transmitter.update_rc_data([l2, r2], [lx, ly], [rx, ry])
        transmitter.send()

        # Check, if data was received correctly
        received_rc = transmitter.mspio.read_rc()
        received_rc = [received_rc['roll'], received_rc['pitch'], received_rc['yaw'], received_rc['throttle'], received_rc['aux1'], received_rc['aux2'], received_rc['aux3'], received_rc['aux4']]
        print("Sent RC: {0}, Received RC: {1}".format(transmitter.rc_data, received_rc), end='\r')

        # TODO
        #print(transmitter.get_pin_output())

        # Sleep for our polling interval
        time.sleep(pollInterval)
finally:
    # Ensure the background thread is always terminated, when we are done
    gamepad.disconnect()

    # For safety reasons, make sure, that drone is entering failsafe mode, when we are done
    transmitter.connection_loss_handler()