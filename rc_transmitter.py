from tkinter import *
import sys
import time
import RPi.GPIO as GPIO
import numpy as np

from MSPio.MultiWiiProtocol import MSPio

class RCTransmitter:
    # Parameters
    # step_size: steps within range [1000,2000] taken after each polling interval
    # len_range: range around midpoint 1500 for ROLL/PITCH/YAW
    # min_throttle: minimal throttle as in the flight configurator
    # max_throttle: maximal throttle as in the flight configurator (or smaller for testing)
    def __init__(self, step_size=5, len_range=[500, 500, 500], min_throttle=1150, max_throttle=2000, hover_throttle=1460) -> None:
        # Initialize connection to FC
        self.mspio = MSPio()
        print("\nGive the FC a couple secs to fully init...")
        start = time.time()
        while time.time() - start < 2:
            # Send disarm sequence just in case
            self.mspio.set_raw_rc([1500, 1500, 1000, 1000] + [0, 0, 0, 0])
            time.sleep(0.5)

        self._check_connection()
        self.mspio._serial.read_all()

        # Resulting range for Roll/Pitch/Yaw is [1500 - len_range[i]/2, 1500 + len_range[i]/2]
        self.LEN_RANGE          = len_range
        self.STEP_SIZE          = step_size
        self.MIN_THROTTLE       = min_throttle
        self.MAX_THROTTLE       = max_throttle
        self.HOVER_THROTTLE     = hover_throttle

        # ROLL/PITCH/YAW/THROTTLE + AUX1/AUX2/AUX3/AUX4
        # Idle: Centered joysticks and zero Throttle
        self.IDLE               = [1500, 1500, 1500, 1000]
        self._rc_data           = self.IDLE + [0, 0, 0, 0]

        # Coefficients of a polynomial of degree 3 used for the non-linear mapping of the throttle
        # p[0] * x**3 + p[1] * x**2 + p[2] * x + p[3]
        self.POL_COEFF          = [0.7, 0, 3, 0]

        # Flag to check, whether the FC entered failsafe mode in order to respond correspondingly
        self._in_failsafe_mode  = False
        # Flag to ensure, that we only arm, when disarmed and vice versa
        self._armed             = False

        # TODO: buggy
        # GPIO pin setup to read off whether the Pi's battery voltage is sufficient
        GPIO.setmode(GPIO.BCM) # using BCM numeration (logical GPIO number, like GPIO 4, and not physical pin number)
        GPIO.setup(4, GPIO.OUT) # set GPIO 4 to be output

    # ------------------------------ GETTER ------------------------------

    @property
    def armed(self) -> bool:
        return self._armed
    
    @property
    def rc_data(self) -> list:
        return self._rc_data

    @property
    def in_failsafe_mode(self) -> bool:
        return self._in_failsafe_mode

    # -------------------------- PRIVATE METHODS --------------------------

    # Always check, if connection to FC exists, before sending anything
    def _check_connection(self) -> None:
        if not self.mspio.is_open():
            self._in_failsafe_mode = True
            print("\nFC is not connected to Pi.")
            self.connection_loss_handler()

    # Maps range in_range = [in_min, in_max] to range out_range = [out_min, out_max]
    def _map_range(self, x: float, in_range: list, out_range: list) -> float:
        return (x - in_range[0]) * (out_range[1] - out_range[0]) / (in_range[1] - in_range[0]) + out_range[0]

    def _pol(self, x):
        return (self.POL_COEFF[0]*(x**3)) + (self.POL_COEFF[1]*(x**2)) + (self.POL_COEFF[2]*x) + self.POL_COEFF[3]

    # Maps stick position in [0, 1] to a non-linear throttle output
    def _map_stick_pos(self, pos, hover_stick_pos=0.4, right=1.0, left=-2.0*3):
        acc_off = 1e-3

        # defines bounding box around polynomial
        up = self._pol(right)
        down = self._pol(left)

        if pos <= hover_stick_pos:
            mapped_pos = self._map_range(
                x           = pos,
                in_range    = [0.0, hover_stick_pos],
                out_range   = [left, 0.0]
            )
            out_throttle = int(self._map_range(
                x           = self._pol(mapped_pos),
                in_range    = [down, 0.0],
                out_range   = [self.MIN_THROTTLE, self.HOVER_THROTTLE]
            ))
        else:
            mapped_pos = self._map_range(
                x           = pos,
                in_range    = [hover_stick_pos+acc_off, 1.0],
                out_range   = [0.0+acc_off, right]
            )
            out_throttle = int(self._map_range(
                x           = self._pol(mapped_pos),
                in_range    = [0.0+acc_off, up],
                out_range   = [self.HOVER_THROTTLE+acc_off, self.MAX_THROTTLE]
            ))
        return out_throttle

    # -------------------------- PUBLIC METHODS --------------------------

    # Bring drone to the ground safely (failsafe mode), when connection to controller is lost
    def connection_loss_handler(self) -> None:
        print('\nTransmitter lost connection to FC.')
        print('Cleaning up...')
        self.mspio.cleanup()
        self.mspio.close()
        sys.exit(0)

    # Roll and Pitch controlled through the left/right gamepad joystick
    def update_roll_pitch(self, pos_stick: list) -> None:
        # FC only acknowledges send RC data, if drone is armed
        if not self._armed:
            return

        # Ensure, that the values lie within the expected range of [-1,1]
        pos_stick = [max(min(p, 1), -1) for p in pos_stick]

        # Compute new roll and pitch values proportional to the maximal change
        self._rc_data[0] = 1500 + int(pos_stick[0] * (self.LEN_RANGE[0]/2))
        self._rc_data[1] = 1500 + int(pos_stick[1] * (self.LEN_RANGE[1]/2))

    # Throttle is controlled through the left joystick
    def update_throttle(self, pos_stick: list) -> None:
        # FC only acknowledges send RC data, if drone is armed
        if not self._armed:
            return

        # Ensure, that the values lie within the expected range of [-1,1]
        pos_stick = [max(min(p, 1), -1) for p in pos_stick]

        # Throttle can only be increased; If stick jumps to (0,0), it should decrease up to minimum
        # Instead of linear mapping, we use a non linear one in order to increase the sensitive throttle range 
        # around the hover throttle
        if pos_stick[1] <= 0:
            self._rc_data[3] = self.MIN_THROTTLE
        else:
            # Hold RC value according to stick position
            self._rc_data[3] = self._map_stick_pos(pos_stick[1])

    # Yaw is controlled by the L2/R2 (lower) shoulder buttons on the gamepad
    # They act as buttons as well as axes
    # R2 to turn clockwise, L2 to turn counterclockwise
    def update_yaw_lower(self, l2_r2: list) -> None:
        # FC only acknowledges send RC data, if drone is armed
        if not self._armed:
            return

        l2 = self._map_range(l2_r2[0], [-1, 1], [0, 1])
        r2 = self._map_range(l2_r2[1], [-1, 1], [0, 1])

        self._rc_data[2] = 1500 - int(l2 * (self.LEN_RANGE[2]/2)) + int(r2 * (self.LEN_RANGE[2]/2))

    # Updates RC data according to the game control mapping
    def update_rc_data(self, l2_r2: list, pos_stick_l: list, pos_stick_r: list) -> None:
        # FC only acknowledges send RC data, if drone is armed
        if not self._armed:
            return

        self.update_yaw_lower(l2_r2)
        self.update_throttle(pos_stick_l)
        self.update_roll_pitch(pos_stick_r)

    # TODO: buggy
    def get_pin_output(self) -> int:
        return GPIO.input(4)

    # Activates/deactivates angle mode on AUX2 (high/low)
    def angle_mode(self) -> bool:
        if self._rc_data[5] < 2000:
            self._rc_data[5] = 2000
            return True
        else:
            self._rc_data[5] = 1000
            return False

    # -------------------------- METHODS FOR COMMUNICATION WITH FC --------------------------

    def arm(self):
        self._check_connection()

        # Only arm, if drone is not yet armed
        if self._armed:
            print("\nAlready armed!")
            return

        print("\nArming...")

        # Arming is done via AUX1
        self._rc_data[:4] = self.IDLE
        self._rc_data[4] = 2000

        self.mspio.switch_arm()

        print("Arming done.")

        self._armed = True

    def disarm(self):
        self._check_connection()

        # Only disarm, when drone is armed and throttle is at its minimum
        if not self._armed:
            print("\nAlready disarmed!")
            return
        if self._rc_data[3] != self.MIN_THROTTLE:
            print("\nBring drone to the ground, before disarming!")
            return

        print("\nDisarming...")

        # Disarming is done via AUX1
        self._rc_data[:4] = self.IDLE
        self._rc_data[4] = 1000

        self.mspio.switch_disarm()

        print("Disarming done.")

        self._armed = False
    
    # Send stored RC data to FC
    def send(self):
        self._check_connection()
        self.mspio.set_raw_rc(self._rc_data)
