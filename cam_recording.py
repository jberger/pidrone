import os
from time import sleep
from picamera import PiCamera

cam = PiCamera()

try:
    PATH_TO_IMG = str(input('Output directory for images (ex: output): '))
    POLLING = float(input('Polling interval (in sec): '))
    WIDTH = int(input('Image width (<= 2592): '))
    HEIGHT = int(WIDTH * (1944/2592)) # keep same ratio as in the original resolution
    
    if not os.path.exists(PATH_TO_IMG):
        os.makedirs(PATH_TO_IMG)

    #cam.start_preview()
    for filename in cam.capture_continuous(PATH_TO_IMG + '/img{counter}.jpg', resize=(WIDTH, HEIGHT)):
        sleep(POLLING)

except ValueError:
    print('Invalid input!')
finally:
    #cam.stop_preview()
    cam.close()
