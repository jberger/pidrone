# PiDrone

Library to operate a drone equipped with an on-board Raspberry Pi, that is connected to a MultiWii-based flight controller via USB, and connected to a gamepad via Bluetooth.

The library was tested on the flight controller Naze32 Rev5. 

# Testing

**WARNING: Make sure to remove the propellers and unplug the main battery, when running tests!**

## Readings

To test the readings, call

    python tests/test_readings.py

and type in the desired option (`att` or `imu`).

For `att`:
- `angy`: roll in degrees
- `angx`: pitch in degrees
- `heading`: yaw in degrees

For `imu`:
- `accx`/`accy`,`accz`: 3-axis accelerometer readings
- `gyrx`/`gyry`/`gyrz`: 3-axis gyroscope readings
- `magx`/`magy`/`magz`: 3-axis magnetometer readings


## Arming and Disarming

Use this test to check, whether every component in the drone is working correctly.

1. Place the drone away from you on a flat ground.
2. On the Pi connected to the FC via USB, run from `PiDrone`

        python MSPio/MultiWiiProtocol.py       

    The FC will arm for a few seconds, and disarm afterwards.

_Note:_ The program assumes, that arm/disarm is set on stick commands (min. throttle, max. yaw). If arming was set on AUX1 in the FC configurator, the arm/disarm sequence in `MultiWiiProtocol.py` should be changed.

If you notice, that the motors are acting up, the minimal throttle is probably set too low. Change the minimal throttle on the FC in the configurator and in `rc_transmitter` accordingly.

## Gamepad Commands

Possible adaptations needed:
- `gamepad_to_rc.py`:       
    - Change gamepad type and controls to match your specific gamepad. See `Gamepad/README.md` for more information.
- `rc_transmitter.py`:
    - You might change `MIN_THROTTLE` and `MAX_THROTTLE` to their values defined in the FC configurator. 
    - When using the AUX channels, all the sequence lengths might need adaption.
    - When arm/disarm is set on AUX1, change `ARMING_SEQUENCE` and `DISARMING_SEQUENCE`.


To test, whether the communication gamepad → Pi → FC works flawlessly, run

    python gamepad_to_rc.py

The program sends the received commands from the gamepad to the FC. Make sure, that sent RC and received RC match.

# Advanced Adaptation
## MultiWiiProtocol.py
### Adding New Functions

When implementing a new function for a not yet supported command found in [the MSP documentation](http://www.multiwii.com/wiki/index.php?title=Multiwii_Serial_Protocol):

1. Add command name and message ID to the list of supported codes.
2. If this command is a request to the FC (indicated through "FC →"), we additionally require a suitable parser scheme, which is used to unpack answer we receive from the FC. This answer is a string of binary representations, which needs to be converted to its original form according to a specified format determined by the parser. The format specifies, how many values of which type are supposed to be in the binary string. See the comments in `MutiWiiProtocol.py` for further information on how to construct the parser.
3. In the actual function implementation, you send your new command, and immidiately read the response of the FC, when a response is expected. 

    A example for a possible new request function:

        def new_read_func(self):
            self.send_cmd(<command_name>)
            tmp, status_ok = self.read_response(self.<command_name>, self.<parser_name>)
            if status_ok:
                # Extract values from tmp
            return ...

    A example for a possible new command function:

        def new_cmd_func(self, input):
            # Check, if input is valid
            [...]
            self.send_cmd(self.<command_name>, input, <input_size_in_bytes>)
